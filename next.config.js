/** @type {import('next').NextConfig} */
const nextConfig = {
    images: {
        domains: ["images.freeimages.com"],
      },
}

module.exports = nextConfig
