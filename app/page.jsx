import Image from "next/image";
import styles from "./page.module.css";

export default function Home() {
    return (
        <>
            <h2>Welcome to your blogs.</h2>
            <main className={styles.main}>
                <div className={styles.mainDesc}>
                    <p>
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                        Cras luctus lectus augue, id gravida sapien ultrices
                        vel. Ut semper eros a lorem vestibulum condimentum.
                        Etiam pellentesque sit amet lectus id venenatis. Ut
                        blandit, tortor non elementum vestibulum, est nisl
                        pulvinar ante, laoreet egestas mi lectus nec felis.
                        Aliquam sed viverra lectus, eget feugiat tellus. Nulla
                        aliquet tristique lectus quis sollicitudin. Proin
                        pulvinar enim eget molestie volutpat. Ut vitae
                        scelerisque quam, at elementum justo. In et maximus
                        augue. Maecenas tincidunt ante vitae suscipit semper.
                        Duis sit amet fringilla mi, non viverra sem. Phasellus
                        fringilla vehicula condimentum. Pellentesque vestibulum
                        congue scelerisque. Morbi a turpis vitae ipsum sagittis
                        tempor eu eu arcu. Nam eget massa tortor.
                    </p>
                </div>
                <div className={styles.mainImg}>
                    <Image
                        src="https://images.freeimages.com/vhq/images/previews/3c7/desktop-vector-elements-561434.jpg"
                        alt="banner image"
                        fill={true}
                    />
                </div>
            </main>
        </>
    );
}
