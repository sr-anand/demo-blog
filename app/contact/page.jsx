import ContactForm from "@/components/Contact/ContactForm";
import React from "react";

const ContactUs = () => {
    return (
        <div>
            <h2>Contact us</h2>
            <ContactForm />
        </div>
    );
};

export default ContactUs;
