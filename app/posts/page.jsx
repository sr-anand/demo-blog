import React from "react";
import styles from "./page.module.css";
import { getPosts } from "@/services/postServices";
import Post from "@/components/Post/Post";

const Posts = async () => {
    const fetchedPosts = await getPosts();
    const posts = fetchedPosts.slice(0, 10);
    return (
        <>
            <h2>Posts</h2>
            <div className={styles.posts}>
                {posts.map((post) => (
                    <Post key={post.id} post={post} />
                ))}
            </div>
        </>
    );
};

export default Posts;
