import { getSinglePost } from '@/services/postServices'
import React from 'react'
import styles from './page.module.css'

const PostDetails = async ({params}) => {
  const post = await getSinglePost(params.postId)

  return (
    <div className={styles.post}>
      <h2>{post.title}</h2>
      <p>{post.body}</p>
    </div>
  )
}

export default PostDetails