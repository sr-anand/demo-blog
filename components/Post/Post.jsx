"use client";
import React from "react";
import styles from "./post.module.css";
import { useRouter } from "next/navigation";

const Post = ({ post }) => {
    const router = useRouter();

    function handleClick(id) {
        router.push(`/posts/${id}`);
    }

    return (
        <div
            className={styles.post}
            onClick={() => {
                handleClick(post.id);
            }}
        >
            <h3>{post.title}</h3>
            <p>{post.body}</p>
        </div>
    );
};

export default Post;
