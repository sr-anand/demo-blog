"use client";
import React from "react";
import styles from "./ContactForm.module.css";
import { useFormik } from "formik";
import * as Yup from "yup";
import Button from "../Button/Button";

const ContactForm = () => {
    const formik = useFormik({
        initialValues: {
            issue: "",
            email: "",
            desc: "",
        },
        validationSchema: Yup.object().shape({
            email: Yup.string()
                .matches(
                    /^[\w-]+(\.[\w-]+)*@([\w-]+\.)+[a-zA-Z]{2,7}$/,
                    "Invalid email address"
                )
                .required("Please enter email"),
            issue: Yup.string().required("Please enter the issue"),
        }),
        onSubmit: (values) => {
            console.log(values);
        },
    });

    return (
        <>
            <form
                action=""
                className={styles.form}
                onSubmit={formik.handleSubmit}
            >
                <label htmlFor="issue">
                    Issues <span>*</span>
                </label>
                <input
                    type="text"
                    id="issue"
                    name="issue"
                    onChange={formik.handleChange}
                    onBlur={formik.handleBlur}
                    value={formik.values.issue}
                />
                {formik.touched.issue && formik.errors.issue ? (
                    <span className="error">{formik.errors.issue}</span>
                ) : null}
                <label htmlFor="email">
                    Your Email <span>*</span>
                </label>
                <input
                    type="text"
                    id="email"
                    name="email"
                    onChange={formik.handleChange}
                    onBlur={formik.handleBlur}
                    value={formik.values.email}
                />
                {formik.touched.email && formik.errors.email ? (
                    <span className="error">{formik.errors.email}</span>
                ) : null}
                <label htmlFor="desc">Your Email</label>
                <textarea
                    rows="10"
                    id="desc"
                    name="desc"
                    onChange={formik.handleChange}
                    onBlur={formik.handleBlur}
                    value={formik.values.desc}
                />
                <Button
                    type={"submit"}
                    text={"Submit"}
                    onClick={formik.handleSubmit}
                />
            </form>
        </>
    );
};

export default ContactForm;
