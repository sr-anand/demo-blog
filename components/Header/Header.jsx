import React from "react";
import styles from "./Header.module.css";
import Navlist from "./Navlist";
import Link from "next/link";

const Header = () => {
    return (
        <div className={styles.header}>
            <div className={styles.logo}>
              <Link href='/'>
                <h1>LOGO</h1>
              </Link>
            </div>
            <div className={styles.nav}>
                <ul>
                  <Navlist />
                </ul>
            </div>
        </div>
    );
};

export default Header;
