import React from "react";
import styles from "./Header.module.css";
import Link from "next/link";

const Navlist = () => {
    return (
        <div className={styles.navlist}>
            <li>
                <Link href="/posts">Posts</Link>
            </li>
            <li>
                <Link href="/contact">Contact Us</Link>
            </li>
            <li>
                <Link href="/dashboard">Dashboard</Link>
            </li>
        </div>
    );
};

export default Navlist;
