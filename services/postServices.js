import axios from "axios";

export async function getPosts() {
  try {
    const res = await axios.get('https://jsonplaceholder.typicode.com/posts');

    if (res.status !== 200) {
      throw new Error('Failed to fetch data');
    }

    return res.data;
  } catch (error) {
    throw new Error('Failed to fetch data');
  }
}

export async function getSinglePost(id) {
  try {
    const res = await axios.get(`https://jsonplaceholder.typicode.com/posts/${id}`);

    if (res.status !== 200) {
      throw new Error('Failed to fetch data');
    }

    return res.data;
  } catch (error) {
    throw new Error('Failed to fetch data');
  }
}
